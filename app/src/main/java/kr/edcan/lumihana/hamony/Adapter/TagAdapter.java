package kr.edcan.lumihana.hamony.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.Data.TagData;
import kr.edcan.lumihana.hamony.R;

/**
 * Created by kimok_000 on 2016-07-14.
 */
public abstract class TagAdapter extends RecyclerView.Adapter<TagAdapter.ViewHolder> {
    private Context context;
    private ArrayList<TagData> arrayList;
    private int itemLayout;

    public abstract void onCancel(int position);

    public TagAdapter(Context context, ArrayList<TagData> arrayList, int itemLayout) {
        this.context = context;
        this.arrayList = arrayList;
        this.itemLayout = itemLayout;
    }

    @Override
    public TagAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_debate, null);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TagAdapter.ViewHolder holder, final int position) {
        TagData data = arrayList.get(position);

        holder.text_tag.setText(data.getTag().toString().trim());
        holder.image_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancel(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView text_tag;
        ImageView image_cancel;

        public ViewHolder(View itemView) {
            super(itemView);

            text_tag = (TextView) itemView.findViewById(R.id.debate_text_tag);
            image_cancel = (ImageView) itemView.findViewById(R.id.debate_image_cancel);
        }
    }
}
