package kr.edcan.lumihana.hamony.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kr.edcan.lumihana.hamony.Adapter.CommentAdapter;
import kr.edcan.lumihana.hamony.Data.CommentData;
import kr.edcan.lumihana.hamony.Data.Data;
import kr.edcan.lumihana.hamony.R;
import kr.edcan.lumihana.hamony.Utils.AdapterTool;

/**
 * Created by rnd33 on 2016-07-13.
 */
public class CommentActivity extends AppCompatActivity implements AdapterTool{
    private ArrayList<CommentData> arrayList;
    private CommentAdapter adapter;
    private ListView listView;
    private EditText edit_comment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        listView = (ListView) findViewById(R.id.comment_list_comments);
        edit_comment = (EditText) findViewById(R.id.comment_edit_comment);

        initList();
        addList(new CommentData("최원경", "asdfasdfsdfsdfasdfasdfadfasdfadssdfsdfasfadfsdfadsffsdafsdfasdfasfasfdsfafsdfsdfsdfaafadsfsfasffsdasdfasdffsdfafadssdf", CommentData.COMMENT_FAVORITE));
        addList(new CommentData("최원경", "붸에에에에에", CommentData.COMMENT_FAVORITE));
        addList(new CommentData("최원경", "붸에에에에에", CommentData.COMMENT_FAVORITE));
        addList(new CommentData("최원경", "붸에에에에에", CommentData.COMMENT_FAVORITE));
        addList(new CommentData("최원경", "붸에에에에에", CommentData.COMMENT_FAVORITE));
        addList(new CommentData("최원경", "붸에에에에에", CommentData.COMMENT_FAVORITE));
        addList(new CommentData("최원경", "붸에에에에에", CommentData.COMMENT_FAVORITE));
        addList(new CommentData("최원경", "붸에에에에에", CommentData.COMMENT_FAVORITE));
        addList(new CommentData("최원경", "붸에에에에에", CommentData.COMMENT_FAVORITE));

        edit_comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                Pattern pattern = Pattern.compile("(#\\w+\\s+)+");
                Matcher matcher = pattern.matcher(s.toString());
                if(matcher.matches()) {
                    String[] temp = s.toString().split(" ");
                    for(String str : temp){
                        Log.e("hello", str);
                    }
                }
            }
        });
    }

    @Override
    public void initList() {
        arrayList = new ArrayList<>();
        setList();
    }

    @Override
    public void addList(Data data) {
        arrayList.add((CommentData) data);
        setList();
    }

    @Override
    public void setList() {
        adapter = new CommentAdapter(getApplicationContext(), arrayList);
        listView.setAdapter(adapter);
    }
}
