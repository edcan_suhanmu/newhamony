package kr.edcan.lumihana.hamony.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Comment;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.Data.CommentData;
import kr.edcan.lumihana.hamony.R;

/**
 * Created by rnd33 on 2016-07-13.
 */
public class CommentAdapter extends ArrayAdapter<CommentData> {
    private Context context;
    private LayoutInflater inflater;

    public CommentAdapter(Context context, ArrayList<CommentData> arrayList) {
        super(context, 0, arrayList);

        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        if(view != null) view = convertView;
        else view = inflater.inflate(R.layout.content_comment, null);

        final CommentData data = this.getItem(position);

        if(data != null){
            TextView text_name = (TextView) view.findViewById(R.id.comment_text_name);
            TextView text_content = (TextView) view.findViewById(R.id.comment_text_content);
            TextView text_profile = (TextView) view.findViewById(R.id.comment_text_profile);
            final ImageView image_favorite = (ImageView) view.findViewById(R.id.comment_image_favorite);

            if(data.getName() == null) {
                text_name.setText(R.string.text_anonymous);
                text_profile.setText(R.string.text_anonymous);
            }
            else {
                text_name.setText(data.getName());
                text_profile.setText(data.getName());
            }

            if(data.getContent() == null) text_content.setText(R.string.text_empty);
            else text_content.setText(data.getContent());

            if(data.isfavorite()) image_favorite.setImageResource(R.drawable.ic_favorited);
            else image_favorite.setImageResource(R.drawable.ic_notfavorited);

            image_favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(data.isfavorite()) {
                        data.setIsfavorite(CommentData.COMMENT_NOT_FAVORITE);
                        image_favorite.setImageResource(R.drawable.ic_notfavorited);
                    }else{
                        data.setIsfavorite(CommentData.COMMENT_FAVORITE);
                        image_favorite.setImageResource(R.drawable.ic_favorited);
                    }
                }
            });
        }

        return view;
    }
}
