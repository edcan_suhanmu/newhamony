package kr.edcan.lumihana.hamony.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kr.edcan.lumihana.hamony.Adapter.TagAdapter;
import kr.edcan.lumihana.hamony.Data.Data;
import kr.edcan.lumihana.hamony.Data.TagData;
import kr.edcan.lumihana.hamony.R;
import kr.edcan.lumihana.hamony.Utils.IAdapterTool;
import kr.edcan.lumihana.hamony.Utils.RecyclerViewDecoration;

/**
 * Created by kimok_000 on 2016-06-30.
 */
public abstract class DebateDialog extends Dialog implements View.OnClickListener, IAdapterTool {
    private EditText edit_title, edit_content;
    private TextView text_name, text_apply, text_cancel;
    private ImageView image_timer;
    private RecyclerView recycler_tags;
    private TagAdapter adapter;
    private LinearLayoutManager layoutManager;
    private ArrayList<TagData> arrayList;

    private String selectedTime = "1시간 후";

    protected abstract void onApply(String title, String content, String name, String selectedTime);

    protected abstract void onCancel();

    public DebateDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.dialog_debate);

        recycler_tags = (RecyclerView) findViewById(R.id.debate_recycler_tags);
        edit_title = (EditText) findViewById(R.id.debate_edit_title);
        edit_content = (EditText) findViewById(R.id.debate_edit_content);
        text_name = (TextView) findViewById(R.id.debate_text_name);
        text_apply = (TextView) findViewById(R.id.dabate_text_apply);
        text_cancel = (TextView) findViewById(R.id.debate_text_cancel);
        image_timer = (ImageView) findViewById(R.id.debate_image_timer);

        text_apply.setOnClickListener(this);
        text_cancel.setOnClickListener(this);
        image_timer.setOnClickListener(this);

        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recycler_tags.setHasFixedSize(true);
        recycler_tags.setLayoutManager(layoutManager);
        recycler_tags.addItemDecoration(new RecyclerViewDecoration(8));

        edit_content.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Pattern pattern = Pattern.compile("(#\\w+\\s+)+");
                Matcher matcher = pattern.matcher(s.toString());
                if(matcher.matches()){
                    Log.e("hello", s.toString());
                    initList();
                    String[] temp = s.toString().split(" ");
                    for(String str : temp){
                        if(!str.trim().equals("")) addList(new TagData(str.trim()));
                    }

                    if(s.toString().trim().equals("")) arrayList = new ArrayList<TagData>();
                }
            }
        });
    }

    @Override
    public void addList(Data data) {
        arrayList.add((TagData) data);
        setList();
    }

    @Override
    public void setList() {
        adapter = new TagAdapter(getContext(), arrayList, R.layout.content_debate) {
            @Override
            public void onCancel(int position) {
                arrayList.remove(position);
                setList();
            }
        };
        recycler_tags.setAdapter(adapter);
    }

    @Override
    public void initList() {
        arrayList = new ArrayList<>();
        setList();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dabate_text_apply: {
                String name = text_name.getText().toString().trim();
                String title = edit_title.getText().toString().trim();
                String content = edit_content.getText().toString().trim();

                if (name.equals("") || title.equals("") || content.equals("")) {
                    Toast.makeText(getContext(), "입력 내용을 확인해주세요", Toast.LENGTH_SHORT).show();
                    return;
                }

                onApply(title, content, name, selectedTime);
                break;
            }
            case R.id.debate_text_cancel:
                onCancel();
                break;
            case R.id.debate_image_timer: {


                break;
            }
        }
    }
}
