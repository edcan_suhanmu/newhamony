package kr.edcan.lumihana.hamony.Utils;

import kr.edcan.lumihana.hamony.Data.Data;

/**
 * Created by kimok_000 on 2016-07-09.
 */
public interface IAdapterTool {
    void initList();
    void addList(Data data);
    void setList();
}
