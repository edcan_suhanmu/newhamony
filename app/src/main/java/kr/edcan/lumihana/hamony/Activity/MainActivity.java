package kr.edcan.lumihana.hamony.Activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.Adapter.CardPostAdapter;
import kr.edcan.lumihana.hamony.Data.CardPostData;
import kr.edcan.lumihana.hamony.Data.Data;
import kr.edcan.lumihana.hamony.Dialog.DebateDialog;
import kr.edcan.lumihana.hamony.Dialog.SelectionDialog;
import kr.edcan.lumihana.hamony.R;
import kr.edcan.lumihana.hamony.Utils.IAdapterTool;
import kr.edcan.lumihana.hamony.Utils.RecyclerViewDecoration;

public class MainActivity extends AppCompatActivity implements IAdapterTool {
    private RecyclerView recycler_contents;
    private CardPostAdapter adapter;
    private ArrayList<CardPostData> arrayList;
    private GridLayoutManager layoutManager;
    private RecyclerViewDecoration decoration;
    private FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recycler_contents = (RecyclerView) findViewById(R.id.main_recycler_contents);

        layoutManager = new GridLayoutManager(getApplicationContext(), GridLayoutManager.VERTICAL);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.main_fab);
        decoration = new RecyclerViewDecoration(16);
        recycler_contents.setHasFixedSize(true);
        recycler_contents.setLayoutManager(layoutManager);
        recycler_contents.addItemDecoration(decoration);

        initList();
        addList(new CardPostData("가을 방학은 가을 방학이야", null, CardPostData.TYPE_DEBATE, 23, 5));
        addList(new CardPostData("여름 방학은 여름 방학이야", null, CardPostData.TYPE_DEBATE, 23, 5));
        addList(new CardPostData("가을 방학은 가을 방학이야", null, CardPostData.TYPE_DEBATE, 23, 5));
        addList(new CardPostData("여름 방학은 여름 방학이야", null, CardPostData.TYPE_DEBATE, 23, 5));
        addList(new CardPostData("가을 방학은 가을 방학이야", null, CardPostData.TYPE_DEBATE, 23, 5));
        addList(new CardPostData("여름 방학은 여름 방학이야", null, CardPostData.TYPE_DEBATE, 23, 5));

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SelectionDialog(MainActivity.this) {
                    @Override
                    public void onDebate() {
                        new DebateDialog(MainActivity.this) {
                            @Override
                            protected void onApply(String title, String content, String name, String selectedTime) {

                            }

                            @Override
                            protected void onCancel() {
                                this.dismiss();
                            }
                        }.show();
                    }

                    @Override
                    public void onGoodBad() {

                    }

                    @Override
                    public void onVote() {

                    }
                }.show();
            }
        });

    }


    @Override
    public void initList() {
        arrayList = new ArrayList<>();
        setList();
    }

    @Override
    public void addList(Data data) {
        arrayList.add((CardPostData) data);
        setList();
    }

    @Override
    public void setList() {
        if (arrayList != null) {
            adapter = new CardPostAdapter(getApplicationContext(), arrayList, R.layout.content_main);
            recycler_contents.setAdapter(adapter);
        } else initList();
    }
}