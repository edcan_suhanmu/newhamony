package kr.edcan.lumihana.hamony.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.Data.CardPostData;
import kr.edcan.lumihana.hamony.R;

/**
 * Created by kimok_000 on 2016-06-21.
 */
public class CardPostAdapter extends RecyclerView.Adapter<CardPostAdapter.ViewHolder> {
    private Context context;
    private ArrayList<CardPostData> arrayList;
    private int itemLayout;

    public CardPostAdapter(Context context, ArrayList<CardPostData> arrayList, int itemLayout) {
        this.context = context;
        this.arrayList = arrayList;
        this.itemLayout = itemLayout;
    }

    @Override
    public CardPostAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_main, null);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CardPostAdapter.ViewHolder holder, int position) {
        final CardPostData data = arrayList.get(position);
        holder.image_background.setImageBitmap(data.getImage());
        holder.text_comment.setText(data.getCommentCount() + " / " + data.getTime() + "분전");
        holder.text_content.setText(data.getContent().toString().trim());

        switch (data.getType()){
            case CardPostData.TYPE_DEBATE : {
                holder.text_type.setText("토론");
                break;
            }

            default: {
                holder.text_type.setText("존재하지 않는 게시글");
            }
        }
    }

    @Override
    public int getItemCount() {
        return this.arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout linear_type, linear_comment;
        private ImageView image_background;
        private TextView text_type, text_comment, text_content;

        public ViewHolder(View itemView) {
            super(itemView);

            linear_type = (LinearLayout) itemView.findViewById(R.id.main_linear_type);
            linear_comment = (LinearLayout) itemView.findViewById(R.id.main_linear_comment);
            image_background = (ImageView) itemView.findViewById(R.id.main_image_background);
            text_type = (TextView) itemView.findViewById(R.id.main_text_type);
            text_comment = (TextView) itemView.findViewById(R.id.main_text_comment);
            text_content = (TextView) itemView.findViewById(R.id.main_text_content);

            //TODO : Linear ClickListener
        }
    }
}
