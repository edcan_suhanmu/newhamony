package kr.edcan.lumihana.hamony.Data;

/**
 * Created by kimok_000 on 2016-07-14.
 */
public class TagData extends Data {
    private String tag;

    public TagData(String tag) {
        this.tag = tag;
    }

    public String getTag(){
        return tag;
    }
}
