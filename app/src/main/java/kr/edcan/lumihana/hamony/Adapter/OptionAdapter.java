package kr.edcan.lumihana.hamony.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.Data.OptionData;
import kr.edcan.lumihana.hamony.R;

/**
 * Created by kimok_000 on 2016-07-14.
 */
public class OptionAdapter extends ArrayAdapter<OptionData> {
    private Context context;
    private LayoutInflater inflater;

    public OptionAdapter(Context context, ArrayList<OptionData> arrayList) {
        super(context, 0, arrayList);

        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View view = null;

        if(view!= null) view = convertView;
        else view = inflater.inflate(R.layout.content_selection, null);

        final OptionData data = this.getItem(position);

        if(data!=null){
            TextView text_title = (TextView) view.findViewById(R.id.selection_text_title);
            TextView text_content = (TextView) view.findViewById(R.id.selection_text_content);

            text_title.setText(data.getTitle().toString().trim());
            text_content.setText(data.getContent().toString().trim());
        }

        return view;
    }
}
