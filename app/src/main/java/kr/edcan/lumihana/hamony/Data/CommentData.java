package kr.edcan.lumihana.hamony.Data;

/**
 * Created by rnd33 on 2016-07-13.
 */
public class CommentData extends Data{
    public static final boolean COMMENT_FAVORITE = true;
    public static final boolean COMMENT_NOT_FAVORITE = false;

    private String name;
    private String content;
    private boolean isfavorite;

    public CommentData(String name, String content, boolean isfavorite) {
        this.name = name;
        this.content = content;
        this.isfavorite = isfavorite;
    }

    public String getName() {
        return name;
    }

    public String getContent() {
        return content;
    }

    public boolean isfavorite() {
        return isfavorite;
    }

    public void setIsfavorite(boolean isfavorite) {
        this.isfavorite = isfavorite;
    }
}
