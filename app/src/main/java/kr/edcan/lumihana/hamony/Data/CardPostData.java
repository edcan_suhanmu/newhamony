package kr.edcan.lumihana.hamony.Data;


import android.graphics.Bitmap;

/**
 * Created by kimok_000 on 2016-07-09.
 */
public class CardPostData extends Data {
    public static final int TYPE_DEBATE = 0;

    private String content;
    private Bitmap image;
    private int type;
    private int commentCount;
    private long time;

    public CardPostData(String content, Bitmap image,int type, int commentCount, long time) {
        this.content = content;
        this.image = image;
        this.type = type;
        this.commentCount = commentCount;
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public int getType() {
        return type;
    }

    public Bitmap getImage(){
        return image;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public long getTime() {
        return time;
    }
}
